cmd_copy-ssl-cert_help() {
    cat <<_EOF
    copy-ssl-cert
        Copy the SSL cert from 'revproxy/letsencrypt/' to 'sslcert/'

_EOF
}

cmd_copy-ssl-cert() {
    local ssl_cert_dir=$CONTAINERS/revproxy/letsencrypt/live/$HOSTNAME
    mkdir -p sslcert
    cp -fL $ssl_cert_dir/fullchain.pem sslcert/
    cp -fL $ssl_cert_dir/privkey.pem sslcert/
}
