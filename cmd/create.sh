cmd_create_help() {
    cat <<_EOF
    create
        Create the container '$CONTAINER'.

_EOF
}

rename_function cmd_create base_cmd_create
cmd_create() {
    local domains network_aliases
    if [[ -n $WEBKEY_DOMAINS ]]; then
        domains=$(echo $WEBKEY_DOMAINS | tr ' ' "\n" | sed -e 's/^/openpgpkey./g')
        network_aliases=$(echo $domains | tr ' ' "\n" | sed -e 's/^/--network-alias /g')
    fi
    base_cmd_create \
        --hostname $HOSTNAME \
        `# capability NET_ADMIN needed for iptables and fail2ban` \
        --cap-add=NET_ADMIN \
        $network_aliases \
        "$@"    # accept additional options

    # add domains to revproxy
    ds @revproxy domains-rm $HOSTNAME
    ds @revproxy domains-add $HOSTNAME $domains
    ds @revproxy del-ssl-cert $HOSTNAME
    ds @revproxy get-ssl-cert $HOSTNAME $domains
}
