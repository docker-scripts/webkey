include(focal)

RUN DEBIAN_FRONTEND=noninteractive \
    apt install --yes postfix fail2ban iptables
RUN apt install --yes swaks pflogsumm

# packages for WKS/WKD
RUN apt install --yes apache2 gpg-wks-server sudo
# maildrop returns an error during installation
RUN apt install --yes maildrop || true