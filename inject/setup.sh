#!/bin/bash -x

source /host/settings.sh

main() {
    setup_postfix
    setup_wkd
    setup_wks
    systemctl restart postfix
}

setup_postfix() {
    postconf \
        "mydomain = $HOSTNAME" \
        "myorigin = $HOSTNAME" \
        "myhostname = $HOSTNAME" \
        "mydestination = $HOSTNAME, localhost"

    # mynetworks
    postconf 'mynetworks = /etc/postfix/mynetworks'
    echo $ALLOWED_NETWORKS | tr ' ' "\n" > /etc/postfix/mynetworks

    # restrictions
    postconf \
        'inet_protocols = ipv4' \
        'smtpd_relay_restrictions = permit_mynetworks, reject' \
        'smtpd_sender_restrictions = permit_mynetworks, reject' \
        'smtpd_client_restrictions = permit_mynetworks, reject' \
        'smtpd_helo_required = yes' \
        'smtpd_helo_restrictions = permit_mynetworks, reject'

    # tls settings
    postconf \
        'smtp_tls_security_level = may' \
        'smtpd_tls_security_level = may' \
        'smtp_tls_note_starttls_offer = yes' \
        'smtp_tls_loglevel = 1' \
        'smtpd_tls_loglevel = 1' \
        'smtpd_tls_received_header = yes' \
        'smtpd_tls_cert_file = /host/sslcert/fullchain.pem' \
        'smtpd_tls_key_file = /host/sslcert/privkey.pem'
}

setup_wkd() {
    # create user webkey
    adduser --system \
            --group \
            --disabled-password \
            --shell /bin/bash \
            --home /host/wks \
            webkey

    # create WKD dir
    mkdir -p /host/wkd
    chmod 2750 /host/wkd
    chown webkey:webkey /host/wkd

    ### setup apache2 for WKD
    adduser www-data webkey
    cat <<EOF > /etc/apache2/conf-available/wkd.conf
Alias /.well-known/openpgpkey /host/wkd
<Directory /host/wkd>
    Require all granted
    Options -Indexes
</Directory>
<Directory ~ "/host/wkd/.*/hu">
    ForceType application/octet-stream
    Header always set Access-Control-Allow-Origin "*"
</Directory>
EOF
    a2enconf wkd
    a2dismod --force autoindex
    a2enmod ssl headers
    a2ensite default-ssl
    a2dissite 000-default
    systemctl restart apache2

    # setup the WKD directory for each domain
    for domain in $WEBKEY_DOMAINS; do
        setup_wkd_dir $domain
    done
    
    # check and fix the WKD directories
    sudo -H -u webkey \
       gpg-wks-server --directory /host/wkd --list-domains > /dev/null
}

setup_wkd_dir() {
    local domain=$1
    local wkd_dir=/host/wkd/$domain
    [[ -d $wkd_dir ]] \
        && chown webkey: -R $wkd_dir \
        && return

    # create the directory structure
    mkdir -p $wkd_dir/hu
    touch $wkd_dir/policy
    local submission_address=keys@$domain
    echo $submission_address > $wkd_dir/submission-address
    chown webkey: -R $wkd_dir

    # generate a gpg key for the submission address
    sudo -H -u webkey \
         gpg --batch --passphrase '' --quick-gen-key $submission_address

    # publish the key of the submission address
    local hashed_userid=$(/usr/lib/gnupg/gpg-wks-client \
                              --print-wkd-hash $submission_address \
                              | cut -d' ' -f1)
    rm -f $wkd_dir/hu/$hashed_userid
    sudo -H -u webkey \
         gpg --export --export-options export-minimal \
         --output $wkd_dir/hu/$hashed_userid \
         $submission_address
    chmod +r $wkd_dir/hu/$hashed_userid
}

setup_wks() {
    # maildrop filter for the 'webkey' account
    [[ -f /host/wks/.mailfilter ]] ||
        cat <<EOF > /host/wks/.mailfilter
logfile maildrop.log
#cc archive/    # debug
to "| gpg-wks-server --directory /host/wkd --receive --send &>>maildrop.log"
EOF
    chmod 600 /host/wks/.mailfilter
    touch /host/wks/maildrop.log
    rm -rf /host/wks/archive
    maildirmake /host/wks/archive
    chown webkey: -R /host/wks/

    ### setup a cron job to expire non confirmed publication requests
    cat <<EOF | sudo -H -u webkey crontab -
42 3 * * * gpg-wks-server --directory /host/wkd --cron
EOF

    # forward to 'webkey' emails arriving for 'keys@...'
    echo 'keys webkey' > /etc/aliases
    postmap /etc/aliases
    
    # enable maildrop processing on postfix
    postconf 'mailbox_command = /usr/bin/maildrop -d ${USER}'
    #postfix reload
}

# call the main function
main "$@"
