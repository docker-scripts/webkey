#!/bin/bash -x
### For more details see:
### https://www.linuxbabe.com/mail-server/block-email-spam-postfix

cat <<EOF > /etc/fail2ban/jail.local
[postfix]
enabled = true
maxretry = 3
bantime = 1h
filter = postfix
logpath = /var/log/mail.log

[postfix-flood-attack]
enabled  = true
bantime  = 30m
filter   = postfix-flood-attack
action   = iptables-multiport[name=postfix, port="http,https,smtp,submission,pop3,pop3s,imap,imaps,sieve", protocol=tcp]
logpath  = /var/log/mail.log
ignoreip = 127.0.0.0/8 172.16.0.0/12
EOF

cat <<EOF > /etc/fail2ban/filter.d/postfix-flood-attack.conf
[Definition]
failregex = lost connection after AUTH from (.*)\[<HOST>\]
ignoreregex =
EOF

sed -i /etc/fail2ban/filter.d/postfix.conf \
    -e '/^mode =/ c mode = aggressive'

systemctl restart fail2ban
