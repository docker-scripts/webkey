#!/bin/bash -i

source settings.sh

cat <<EOF
This test assumes that '$HOSTNAME' is listed at 'WEBKEY_DOMAINS'
on 'settings.sh', and that 'openpgpkey.$HOSTNAME' resolves to the
same IP as '$HOSTNAME' (this server).
If not, then cancel this test (with Ctrl+C), edit 'settings.sh',
and run 'ds make' to rebuild the container.
EOF

set -o verbose

#####

ds exec userdel test1 &>/dev/null
rm -rf test/

:; apt install --yes gpg gpg-wks-client swaks

#
# Create an account for a test user
##

:; mkdir -p test

:; ds exec useradd -m -d /host/test/test1 test1

:; ls -al test/test1/

:; test_address=test1@$HOSTNAME

:; echo $test_address

#
# Store emails to the directory Mail on the home
##

:; cat <<EOF > test/test1/.mailfilter
logfile maildrop.log
to Mail
EOF

:; ds exec chmod 600 /host/test/test1/.mailfilter

:; ds exec touch /host/test/test1/maildrop.log

:; ds exec maildirmake /host/test/test1/Mail

:; ds exec chown test1: -R /host/test/test1/

#
# Create a gpg key for test1
##

:; mkdir -p test/gnupg

:; chmod 700 test/gnupg

:; export GNUPGHOME=test/gnupg

:; gpg --batch --passphrase '' --quick-gen-key $test_address

#
# Get the fingerprint of this key
##

:; gpg --list-keys $test_address

:; key_fpr=$(gpg --list-keys $test_address \
                 | sed -n -e '/^pub/,+1p' \
                 | tail -1)

:; key_fpr=$(echo $key_fpr)

:; echo "'$key_fpr'"

#
# Send a message to submit the key
##

:; submission_address=keys@$HOSTNAME

:; sendmail="swaks -tlso -s $HOSTNAME:10025 -f $test_address -t $submission_address -d-"

:; echo $sendmail

:; gpg_wks_client=/usr/lib/gnupg/gpg-wks-client

:; $gpg_wks_client --create -v $key_fpr $test_address | $sendmail
sleep 3

:; ds exec tail /var/log/mail.log -n 15

:; tail wks/maildrop.log

#
# Send a confirmation message
##

:; confirmation_request=$(ls test/test1/Mail/new/* | tail -1)

:; echo $confirmation_request

:; head $confirmation_request -n 20

:; cat $confirmation_request | $gpg_wks_client --receive -v | $sendmail
sleep 3

:; rm $confirmation_request

:; ds exec tail /var/log/mail.log -n 15

:; tail wks/maildrop.log

#
# Check that the key is already published
##

:; publication_notification=$(ls test/test1/Mail/new/* | tail -1)

:; echo $publication_notification

:; head $publication_notification -n 20

:; $gpg_wks_client --print-wkd-hash $test_address

:; ls -al wkd/$HOSTNAME/hu/

:; $gpg_wks_client --check -v $test_address

#
# Cleanup
##

:; $gpg_wks_client --remove-key --directory wkd/ $test_address

:; ds exec userdel test1

:; rm -rf test/
