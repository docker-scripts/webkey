APP=webkey
PORTS='10025:25'

HOSTNAME='wks.example.org'

# WKD+WKS domains that are served
WEBKEY_DOMAINS='
    example.org
'

# Networks/hosts that are permitted to relay key-submission emails.
# This goes to the postfix setting 'mynetworks'.
ALLOWED_NETWORKS='
    127.0.0.0/8
    172.16.0.0/12
'
