# WebKeyDirectory and WebKeyService container

## About

[Web Key Directories](https://wiki.gnupg.org/WKD) provide an easy way
to discover public keys through HTTPS. They provide an important piece
of infrastructure for improving the user experience about exchanging
secure emails and files.

A Web Key Directory can be built and maintained even manually, however
for a large organization it is recommended to set up a [Web Key
Service](https://wiki.gnupg.org/WKS). A WKS is usually implemented as
part of your mail server, however it is also possible to implement it
as a separate component that cooperates with your mail server.

This container app can be such a separate WKS component that augments
your mail server with WKD/WKS support.

## Setup

### Installation

  - First install `ds` and `revproxy`:
      + https://gitlab.com/docker-scripts/ds#installation
      + https://gitlab.com/docker-scripts/revproxy#installation

  - Then get the scripts: `ds pull webkey`

  - Create a directory for the container: `ds init webkey @wks.example.org`

  - Fix the settings: `cd /var/ds/wks.example.org/ ; vim
    settings.sh`. On `WEBKEY_DOMAINS` list the mail domains that will
    be supported by this container.

  - Make the container: `ds make`

  - Test: `ds play tests/test1.sh`


### Configuration on the mail server

We need to make some configurations on the mail server in order to
integrate it with the WKD+WKS server.

1. First of all, add a DNS record like this:

   ```
   openpgpkey.example.org.  IN  CNAME  wks.example.org.
   ```
   
   It is needed to tell the mail clients that the WKD of the domain
   `example.org` is served by `wks.example.org`.

2. Add a virtual alias from `keys@example.org` to
   `keys@wks.example.org`. It might be done like this:

   ```
   postconf "virtual_alias_maps = hash:/host/config/virtual_alias_maps"
   
   cat << EOF >> /host/config/virtual_alias_maps
   keys@example.org  keys@wks.example.org
   EOF
   
   postmap /host/config/virtual_alias_maps
   
   postfix reload
   ```

   This ensures that key submission emails and submission confirmation
   emails are forwarded to the WKS server.
   
3. Tell the mailserver to send (transport) the mails for the domain
   `wks.example.org` to `smtp:[wks.example.org]:10025`. It may be done
   like this:

   ```
   postconf 'transport_maps = hash:/host/config/transport_maps'
   
   cat << EOF > /host/config/transport_maps
   wks.example.org smtp:[wks.example.org]:10025
   EOF
   
   postmap /host/config/transport_maps
   
   postfix reload
   ```

4. Allow the WKS server to send replies (emails) through the
   mailserver, without authentication, by adding its IP to the list of
   `mynetworks`. It might be done like this:
   
   ```
   postconf 'mynetworks = /host/config/trusted_hosts'
   
   cat << EOF >> /host/config/trusted_hosts
   12.34.56.78
   EOF
   
   postfix reload
   ```

5. The envelope of the emails (replies) sent from the WKS server will
   have a sender address like `webkey@wks.example.org`. This may
   confuse the SPF check of mail providers (in case the emails of the
   clients are forwarded to external addresses). As a result these
   emails may be considered less trusty and may end up as spam.
   
   To prevent this we should configure the mailserver to rewrite this
   address to `keys@example.org`. It might be done like this:

   ```
   postconf 'smtp_generic_maps = hash:/host/config/smtp_generic_maps'
   
   cat << EOF > /host/config/smtp_generic_maps
   webkey@wks.example.org keys@example.org
   EOF
   
   postmap /host/config/smtp_generic_maps
   
   postfix reload
   ```


## Test WKD/WKS

For some basic testing of WKD we can use the tool `gpg-wks-client`:

1. Make sure that it is installed:

   ```
   apt install gpg-wks-client
   /usr/lib/gnupg/gpg-wks-client -h
   ```
   
2. Get the WKD url of the key submission address and check that it exists:

   ```
   alias wkcl='/usr/lib/gnupg/gpg-wks-client -v'
   wkcl --print-wkd-url test@example.org
   wget -qO- https://openpgpkey.example.org/.well-known/openpgpkey/example.org/submission-address
   submission_address=$(wget -qO- https://openpgpkey.example.org/.well-known/openpgpkey/example.org/submission-address)
   wkcl --print-wkd-url $submission_address
   wkcl --check test@example.org
   wkcl --check $submission_address
   ```

> To test WKS, we can use
> [Thunderbird/Enigmail](https://gnupg.org/blog/20170803-web-key-in-enigmail.html).
> Let's say that we have a test email account like `test1@example.org`.
> 
> 1. We can generate a quick GPG key for it like this:
>    
>    ```
>    gpg --batch --passphrase '' --quick-gen-key test1@example.org
>    ```
> 
> 2. Next, we need to create an
>    [identity](https://support.mozilla.org/en-US/kb/using-identities)
>    with this email address (in case we don't have one already).
>    
> 3. Then, on the "Enigmail Key Management" window, we can right-click
>    on this key and select the option "Upload to your provider's Web
>    Key Directory". After a short time we will get an information
>    window saying: "Key(s) sent successfully".
>       
> 4. Soon after that we will get a message from `keys@example.org` with
>    subject: `Confirm your key publication`. When we open this message
>    we will also see a button named "Confirm Request" in the status
>    bar. We click this button and we get an information window saying:
>    "Confirmation email sent".
>       
> 5. Finally, we will receive an encrypted message from
>    `keys@example.org` with subject `Your key has been published`, which
>    confirms that the key has been published successfully to the WKD.
> 
> 6. To check the published key, we can use again:
>    
>    ```
>    /usr/lib/gnupg/gpg-wks-client -v --check test1@example.org
>    ```

The latest version of Thunderbird has droped Enigmail. Its
functionality is supposed to be merged to Thunderbird, but not all of
it. In particular the features that facilitate the interaction with a
WKS server are missing. Let’s hope that it will be added back soon,
but meanwhile we can follow these steps to publish a key:

1. Make sure that `msmtp` is installed: `apt install msmtp`

2. Send a key publishing request like this:

   ```
   gpg --list-keys test1@example.org
   
   /usr/lib/gnupg/gpg-wks-client \
           --create AB97233AD0EB0180882D1227799020EF6FF16876 test1@example.org \
       | msmtp \
           --read-envelope-from --read-recipients \
           --tls=on --auth=on \
           --host=smtp.example.org --port=587 \
           --user=test1@example.org --passwordeval="echo pass1"
   ```

3. When the email with subject "Confirm your key publication" arrives,
   save it as a text file.

4. Send the confirmation email with a command like this:

   ```
   cat Confirm-your-key-publication.eml \
       | /usr/lib/gnupg/gpg-wks-client --receive \
       | msmtp  \
           --read-envelope-from --read-recipients \
           --tls=on --auth=on \
           --host=smtp.example.org --port=587 \
           --user=test1@example.org --passwordeval="echo pass1"
   ```
